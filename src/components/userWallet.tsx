import React, { Component, Fragment } from 'react'
import CurrencyChanger from './items/currencyChanger';
import { MDBRow, MDBCol, MDBIcon , MDBBtn}  from 'mdbreact';
import {Bar,Doughnut,HorizontalBar} from 'react-chartjs-2';

interface Props{

}
interface State{
    charData:any
}
export default class userWallet extends Component<Props,State> {
    constructor(props:Props){
        super(props);
        this.state = {
            charData:{
                labels:['Boston','Weoasdf','Qazvin','Lowell','Cambridge','BedFord'],
                datasets:[
                    {
                        label:'population',
                        data:[
                          400,
                          500,
                          650,
                          800,
                          1200,
                          600,
                        ],
                        defaultFontSize: "14px",
                        width: "400",
                        height: "800",
                        backgroundColor:[
                            "#4285f491",
                            "#fd080894",
                            "#4285f491",
                            "#fd080894",
                            "#4285f491",
                            "#fd080894",
                        ]
                    }
                ]
            }
        }
    }
    render() {
        return (
            <Fragment>
                <MDBRow>
                    <MDBCol className="" size="9">
                        <div className="d-flex justify-content-between card shadow-type1 border_radius-middleCurve noBorder">
                                   <MDBRow>
                                <MDBCol  className="d-flex flex-column justify-content-start" size='6'>
                                    <div className="">
                                        <div className={`selectBox simpleNumeric-left flex-size-60`}>
                                            <select className=" browser-default custom-select">
                                                <option>وضعیت حساب گوگل</option>
                                                <option value="1">سریع</option>
                                                <option value="2">بیشترین کلیک</option>
                                                <option value="3">بیشترین مشاهده</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="mt-4">
                                        <h6 className="font-weight-bold d-flex justify-content-start align-items-center text-right">
                                            <span className="currency_converter-icon ml-3">
                                                <MDBIcon className="text-white" size="lg" icon="lira-sign" />
                                            </span>
                                            لیر ترکیه
                                            </h6>
                                    </div>
                                        <div className="d-flex justify-content-end mt-5 pt-4 ">
                                        <MDBBtn className="shadow-type1 d-flex justify-content-between align-items-center px-5 btn btn-success border_radius-middleCurve text-white" >
                                            <span className="ml-4">شارژ کیف پول</span><MDBIcon icon="plus" />
                                        </MDBBtn>
                                        </div>
                                </MDBCol>
                                <MDBCol size='6'>
                                    <div className="position-relative" style={{height:"250px"}}>
                                        <Bar data={this.state.charData} options={{
                                            responsive: true,
                                            maintainAspectRatio: false,
                                            height:"350"
                                        }} />
                                    </div>
                                </MDBCol>
                                
                                   </MDBRow>
                        </div>
                        <MDBCol size='12' className="p-0">
                            <div className="customTable d-flex flex-column mt-5">
                                <div className="customTable-header text-right d-flex justify-content-between">
                                    <div className="d-flex align-items-center">
                                        <MDBIcon className="cursor-pointer text-primary p-2"  icon="filter" size="lg" />
                                        <span>تراکنش ها</span>
                                        <span className="mr-2 text-primary font_size-sm">همه تراکنش ها</span>
                                    </div>
                                    <div className="d-flex justify-content-between">
                                        <div className="d-flex flex-column ">
                                            <MDBIcon className="cursor-pointer text-danger p-2" far icon="chart-bar" size="lg" />
                                            <span>خروج</span>
                                        </div>
                                        <div className="d-flex flex-column mr-4">
                                            <MDBIcon className="cursor-pointer text-danger p-2" icon="ellipsis-v" size="lg" />
                                            <span>بیشتر</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="customTable_content-type1 text-right mt-3">
                                    <div className="accordion mt-1" >
                                        <div className={`accordion_toggle pb-4 pt-2 bg-primary shadow-type1 `}>
                                            <div className="accordion_toggle-content">
                                                <div className="simpleNumeric d-flex justify-content-between">
                                                    <span className="text-white font_size-sm flex-size-20 text-center"></span>
                                                    <span className="text-white font_size-sm flex-size-20 text-center">نوع تراکنش</span>
                                                    <span className="text-white font_size-sm flex-size-20 text-center">تاریخ</span>
                                                    <span className="text-white font_size-sm flex-size-20 text-center">مبلغ</span>
                                                    <span className="text-white font_size-sm flex-size-20 text-center">نوع پرداخت</span>
                                                    <span className="text-white font_size-sm flex-size-20 text-center">فاکتور</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="customTable-content text-right">
                                    <div className="accordion mt-1" >
                                        <div className={`accordion_toggle shadow-type1`} style={{ borderTopLeftRadius: '13px', borderTopRightRadius: '13px', borderBottomLeftRadius: "1px", borderBottomRightRadius: "1px"}}>
                                            <div className="accordion_toggle-content">
                                                <div className="simpleNumeric d-flex justify-content-between">
                                                    <span className="flex-size-20 text-center">
                                                        <div className="simpleNumeric_number-container pl-4">
                                                            <div className={`simpleNumeric_number mx-auto bg-success `}>
                                                                <span style={{ fontSize: ".9rem" }} className="font-weight-bold simpleNumeric_number-digit ">
                                                                    3
                                                        </span>
                                                            </div>
                                                        </div>
                                                    </span>
                                                    <span className="flex-size-20 text-center">شارژ حساب</span>
                                                    <span className="flex-size-20 text-center">1396/07/12</span>
                                                    <span className="flex-size-20 text-center">3400,000 تومان</span>
                                                    <span className="flex-size-20 text-center">پرداخت انلای</span>
                                                    <span className="flex-size-20 text-center">فاکتور</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="customTable-content text-right">
                                    <div className="accordion mt-1" >
                                        <div className={`accordion_toggle shadow-type1 `}>
                                            <div className="accordion_toggle-content">
                                                <div className="simpleNumeric d-flex justify-content-between">
                                                    <span className="flex-size-20 text-center">
                                                        <div className="simpleNumeric_number-container pl-4">
                                                            <div className={`simpleNumeric_number mx-auto bg-danger `}>
                                                                <span style={{ fontSize: ".9rem" }} className="font-weight-bold simpleNumeric_number-digit ">
                                                                    3
                                                        </span>
                                                            </div>
                                                        </div>
                                                    </span>
                                                    <span className="flex-size-20 text-center">ایجاد کردن کمپین</span>
                                                    <span className="flex-size-20 text-center">1396/07/12</span>
                                                    <span className="flex-size-20 text-center">3400,000 تومان</span>
                                                    <span className="flex-size-20 text-center">.</span>
                                                    <span className="flex-size-20 text-center">فاکتور</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="customTable-content text-right">
                                    <div className="accordion mt-1" >
                                        <div className={`accordion_toggle shadow-type1 `}>
                                            <div className="accordion_toggle-content">
                                                <div className="simpleNumeric d-flex justify-content-between">
                                                    <span className="flex-size-20 text-center">
                                                        <div className="simpleNumeric_number-container pl-4">
                                                            <div className={`simpleNumeric_number mx-auto bg-danger `}>
                                                                <span style={{ fontSize: ".9rem" }} className="font-weight-bold simpleNumeric_number-digit ">
                                                                    3
                                                        </span>
                                                            </div>
                                                        </div>
                                                    </span>
                                                    <span className="flex-size-20 text-center">تمدید کمپین</span>
                                                    <span className="flex-size-20 text-center">1396/07/12</span>
                                                    <span className="flex-size-20 text-center">3400,000 تومان</span>
                                                    <span className="flex-size-20 text-center">.</span>
                                                    <span className="flex-size-20 text-center">فاکتور</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="customTable-content text-right">
                                    <div className="accordion mt-1" >
                                        <div className={`accordion_toggle shadow-type1 `}>
                                            <div className="accordion_toggle-content">
                                                <div className="simpleNumeric d-flex justify-content-between">
                                                    <span className="flex-size-20 text-center">
                                                        <div className="simpleNumeric_number-container pl-4">
                                                            <div className={`simpleNumeric_number mx-auto bg-danger `}>
                                                                <span style={{ fontSize: ".9rem" }} className="font-weight-bold simpleNumeric_number-digit ">
                                                                    3
                                                        </span>
                                                            </div>
                                                        </div>
                                                    </span>
                                                    <span className="flex-size-20 text-center">تمدید کمپین</span>
                                                    <span className="flex-size-20 text-center">1396/07/12</span>
                                                    <span className="flex-size-20 text-center">3400,000 تومان</span>
                                                    <span className="flex-size-20 text-center">پرداخت نقدی</span>
                                                    <span className="flex-size-20 text-center">فاکتور</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="customTable-content text-right">
                                    <div className="accordion mt-1" >
                                        <div className={`accordion_toggle shadow-type1 `}>
                                            <div className="accordion_toggle-content">
                                                <div className="simpleNumeric d-flex justify-content-between">
                                                    <span className="flex-size-20 text-center">
                                                        <div className="simpleNumeric_number-container pl-4">
                                                            <div className={`simpleNumeric_number mx-auto bg-success `}>
                                                                <span style={{ fontSize: ".9rem" }} className="font-weight-bold simpleNumeric_number-digit ">
                                                                    3
                                                        </span>
                                                            </div>
                                                        </div>
                                                    </span>
                                                    <span className="flex-size-20 text-center">شارژ حساب</span>
                                                    <span className="flex-size-20 text-center">1396/07/12</span>
                                                    <span className="flex-size-20 text-center">3400,000 تومان</span>
                                                    <span className="flex-size-20 text-center">نوع تراکنش</span>
                                                    <span className="flex-size-20 text-center">فاکتور</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </MDBCol>
                    </MDBCol>
                    <MDBCol className="mr-auto" size="3">
                        <CurrencyChanger />
                    </MDBCol>
                </MDBRow>
            </Fragment>
        )
    }
}
