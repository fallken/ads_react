import React, { Component } from 'react'
import CardSimple from '../items/cardSimple';
import {Link} from 'react-router-dom';
import HeaderComponent from '../items/headerComponent';

export default class createCampaign extends Component {
    render() {
        return (
            <div>     
                    <HeaderComponent />

                 <label htmlFor="example3" className="text-right w-100 font-weight-bold py-5">هدف خود را از ایجاد این کمپین انتخاب کنید</label>
                 <div className="d-flex flex-wrap justify-content-between">
                     <Link to="calender_home">
                        <CardSimple title="ترافیک وبسایت" isHorizontal={true} classes="flex-fill" />
                     </Link>
                    <CardSimple title="نصب برنامه موبایل" isHorizontal={true} classes="flex-fill" />
                    <CardSimple title="کمپین دیسپلی" isHorizontal={true} classes="flex-fill" />

                     <Link to="calender_home">
                        <CardSimple title="کمپین کلیکی" isHorizontal={true} classes="flex-fill mt-4" />
                     </Link>
                    <CardSimple title="خرید از فروشگاه" isHorizontal={true} classes="flex-fill mt-4" />
                    <CardSimple title="کمپین کلیکی" isHorizontal={true} classes="flex-fill mt-4" />
                    </div>
                 <label htmlFor="example3" className="text-right w-100 font-weight-bold py-5">هدف خود را از ایجاد این کمپین انتخاب کنید</label>
                 <div className="d-flex justify-content-between mb-5">
                     <Link to="calender_home">
                        <CardSimple title="جستجو" classes="flex-fill" />
                     </Link>
                    <CardSimple title="دیسپلی " classes="flex-fill" />
                    <CardSimple title="فروشگاه" classes="flex-fill" />
                    <CardSimple title="فروشگاه" classes="flex-fill" />
                 </div>
                
            </div>
        )
    }
}
