import React, { Component, Fragment } from 'react';
import Accordion from '../items/Accordion';    
import MyTest from '../items/accordion_sets/test';
import CardSimple from '../items/cardSimple';
import SimpleNumeric from '../items/accordion_sets/simpleNumeric';
import HeaderComponent from '../items/headerComponent';
import StateCounter from '../items/stageCounter';
import ProgressBar from '../items/accordion_sets/progressBar';
import KeywordButton from '../items/keywordButton';

export default class campaigns extends Component {
    render() {
        return (
            <div>
                <StateCounter />
                <HeaderComponent />
                <Accordion  border_radius={'12px'} toggle_content={<SimpleNumeric sideOption={<ProgressBar />}  number={5} title="نام کمپین و دامنه جستجو" subject="حالت انتخاب شده" data="کمپین شماره یک جستجو"/>} body_content={<MyTest />} />
                <Accordion toggle_content={<SimpleNumeric sideOption={<ProgressBar fill={8} slots={12} />} digitColor='bg-danger'   number={5} title="نام کمپین و دامنه جستجو" subject="حالت انتخاب شده" data="کمپین شماره یک جستجو"/>} body_content={<MyTest />} />
                <Accordion toggle_content={<SimpleNumeric sideOption={<ProgressBar />}  />} body_content={<MyTest />} />
                <label htmlFor="example3" className="text-right w-100 font-weight-bold pt-3">دامنه جستجو را انتخاب کنید</label>
                <label htmlFor="example3" className="text-right has-text-primary w-100 font-weight-normal mr-3 p-b-3">میخواهید چه کسی تبلیغ شما را ببیند?</label>
                <Accordion toggle_content={<SimpleNumeric digitColor='bg-danger' number={5} title="بودجه" subject="حالت انتخاب شده" data="لیر روزانه 300" />} body_content={<CardSimple />} />
                <Accordion toggle_content={<Fragment>
                  <KeywordButton status="دقیق" btnType="customButton-success" />
                  </Fragment>} body_content={<MyTest />}
                 />
                <Accordion toggle_content={<SimpleNumeric  number={5} title="بودجه" subject="حالت انتخاب شده" data="لیر روزانه 300" />} body_content={<MyTest />} />
                <Accordion toggle_content={<SimpleNumeric  number={5} title="بودجه" subject="حالت انتخاب شده" data="لیر روزانه 300" />} body_content={<MyTest />} />
                <label htmlFor="example3" className="text-right w-100 font-weight-bold pt-3">دامنه جستجو را انتخاب کنید</label>
                <label htmlFor="example3" className="text-right has-text-primary w-100 font-weight-normal mr-3 p-b-3">میخواهید چه کسی تبلیغ شما را ببیند?</label>
                <Accordion toggle_content={<SimpleNumeric   number={5} title="بودجه" subject="حالت انتخاب شده" data="لیر روزانه 300"/>} body_content={<MyTest />} />
                <Accordion toggle_content={<SimpleNumeric  number={5} title="بودجه" subject="حالت انتخاب شده" data="لیر روزانه 300" />} body_content={<MyTest />} />
            </div>
        )
    }
}
