import React, { Component, Fragment } from 'react';
import HeaderComponent from '../items/headerComponent';
import StageCounter from '../items/stageCounter';
import SimpleNumeric from '../items/accordion_sets/simpleNumeric';
import Accordion from '../items/Accordion';    
import AddAdgroupKeyword from '../google/addAdgroupKeyword';
import {MDBRow,MDBCol} from 'mdbreact';
import ProgressBar from '../items/accordion_sets/progressBar';
import {MDBIcon} from 'mdbreact'

export default class createAdgroups extends Component {
    render() {
        return (
            <Fragment >
                 <StageCounter active={2} />
                 <HeaderComponent />
                 <h5 className="font-weight-bold text-right">تنظیمات کلمات کلیدی</h5>
                 <h6 className="font-weight-bolder text-right text-primary">کلمات کلیدی در نتایج جستجوی انها نمایش داده میشود</h6>

                 <h5 className="font-weight-bold text-right mt-5">
                        نوع کلمات کلیدی را انتخاب کنید
                    </h5>
                 <MDBRow >
                    <MDBCol size="4" className="mt-5 mb-3">
                        <div className={`selectBox simpleNumeric-left flex-size-60`}>
                            <select className=" browser-default custom-select">
                                <option>وضعیت حساب گوگل</option>
                                <option value="1">سریع</option>
                                <option value="2">بیشترین کلیک</option>
                                <option value="3">بیشترین مشاهده</option>
                            </select>
                        </div>
                    </MDBCol>
                 </MDBRow>

                 <Accordion has_border_buttom_radius={true} setActive="accordion_active"  className="mt-5"   border_radius={'12px'} toggle_content={<SimpleNumeric  sideOption={<ProgressBar fill={3} />}   number={5} title="تنظیمات گروه کلیدواژه" subject="حالت انتخاب شده" data="کمپین شماره یک " />} body_content={<AddAdgroupKeyword />} />
                 <Accordion has_border_buttom_radius={true} setActive="accordion_active"  className="mt-5" border_radius={'12px'}  toggle_content={<SimpleNumeric  sideOption={<ProgressBar slots={5} fill={3} />}   number={5} title="تنظیمات گروه کلیدواژه" subject="حالت انتخاب شده" data="کمپین شماره یک " />} body_content={<AddAdgroupKeyword />} />
                 <Accordion has_border_buttom_radius={true} setActive="accordion_active"  className="mt-5" border_radius={'12px'}  toggle_content={<SimpleNumeric digitColor="bg-muted"  sideOption={<ProgressBar fill={5} />}   number={5} title="تنظیمات گروه کلیدواژه" subject="حالت انتخاب شده" data="کمپین شماره یک " />} body_content={<AddAdgroupKeyword />} />

                <div className="mt-3">
                        <button className="addFeatureButton">افزودن گروه کلید واژه <MDBIcon icon="plus" className="text-primary mr-2" size="lg"/></button>
                </div>
            </Fragment>
        )
    }
}
