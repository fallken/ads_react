import React, { Component } from 'react'
import { MDBIcon } from "mdbreact";
import { NONAME } from 'dns';
import Accordion from '../items/Accordion';    
import SimpleNumeric from '../items/accordion_sets/simpleNumeric';
import MyTest from '../items/accordion_sets/test';
import KeywordButton from '../items/keywordButton';
import KeyworButton from '../items/keywordButton';

export default class addAdgroupKeyword extends Component {
    render() {
        return (
            <div className="d-flex flex-column w-100  pr-5">
                <div className="form-group p-3">
                    <label htmlFor="example3" className="text-right w-100 font-weight-bold">نام گروه کلمات را وارد کنید<span className="has-text-primary"> &#169; </span></label>
                    <input  className="form-control form-control-lg w-50 custom_input-one" placeholder="نام مورد نظر خود را وارد کنید" type="text" id="example3" />
                    <span className="text-muted text-right justify-content-end mt-3 float-right clearfix">مثال : گروه شماره یک</span>
                </div>
                 <label htmlFor="example3" className="text-right w-100 font-weight-bold p-3">دامنه جستجو را انتخاب کنید</label>

                <div className="d-flex">
                      <div className="card shadow-type1 ml-5 flex-size-50">
                            <h6 className="font-weight-bold text-right">جستجو کلمات کلیدی</h6>
                            <hr />
                            <div className="form-group p-3">
                                <label htmlFor="example3" className="text-right w-100 font-weight-bold">نام گروه کلمات را وارد کنید<span className="has-text-primary"> &#169; </span></label>
                                <div className="input-group">
                                <select id="selectOption" className="customDropDown browser-default custom-select">
                                    <option>استاندارد</option>
                                    <option value="1">سریع</option>
                                    <option value="2">بیشترین کلیک</option>
                                    <option value="3">بیشترین مشاهده</option>
                                </select>
                            </div>


                            </div>
                            <div className="pr-3 "  style={{maxHeight:"15rem",overflowY:"scroll"}}>
                                <h6 className="font-weight-bolder text-right mt-2 pr-3">کلمات کلیدی مرتبط</h6>
                                <hr />
                                <div className="card-narrow justify-content-between align-items-center shadow-type1 card-inline-flex d-flex  mt-1">
                                    <div className="">
                                    <KeywordButton   />
                                    <span className="px-3 font-weight-bold">123</span>
                                    </div>
                                <MDBIcon className="text-primary font-weight-bolder ml-2 pointer" icon="plus" />
                                </div>
                                <div className="card-narrow justify-content-between align-items-center shadow-type1 card-inline-flex d-flex mt-1">
                                    <div className="">
                                    <KeywordButton text="تبلغات مدرن در اینجا" btnType={'customButton-danger'} />
                                    <span className="px-3 font-weight-bold">123</span>
                                    </div>
                                <MDBIcon className="text-primary font-weight-bolder ml-2 pointer" icon="plus" />
                                </div>
                                <div className="card-narrow justify-content-between align-items-center shadow-type1 card-inline-flex d-flex  mt-1">
                                    <div>
                                    <KeywordButton text="تبلیغات خیلی خفن" btnType={'customButton-success'}  />
                                    <span className="px-3 font-weight-bold">123</span>
                                    </div>
                                <MDBIcon className="text-primary font-weight-bolder ml-2 pointer" icon="plus" />
                                </div>
                                <div className="card-narrow justify-content-between align-items-center shadow-type1 card-inline-flex d-flex  mt-1">
                                    <div>
                                    <KeywordButton text="تبلیغات خیلی خفن" btnType={'customButton-success'}  />
                                    <span className="px-3 font-weight-bold">123</span>
                                    </div>
                                <MDBIcon className="text-primary font-weight-bolder ml-2 pointer" icon="plus" />
                                </div>
                                <div className="card-narrow justify-content-between align-items-center shadow-type1 card-inline-flex d-flex  mt-1">
                                    <div>
                                    <KeywordButton text="تبلیغات خیلی خفن" btnType={'customButton-success'}  />
                                    <span className="px-3 font-weight-bold">123</span>
                                    </div>
                                <MDBIcon className="text-primary font-weight-bolder ml-2 pointer" icon="plus" />
                                </div>
                                <div className="card-narrow justify-content-between align-items-center shadow-type1 card-inline-flex d-flex  mt-1">
                                    <div>
                                    <KeywordButton text="تبلیغات خیلی خفن" btnType={'customButton-success'}  />
                                    <span className="px-3 font-weight-bold">123</span>
                                    </div>
                                <MDBIcon className="text-primary font-weight-bolder ml-2 pointer" icon="plus" />
                                </div>
                                <div className="card-narrow justify-content-between align-items-center shadow-type1 card-inline-flex d-flex  mt-1">
                                    <div>
                                    <KeywordButton text="تبلیغات خیلی خفن" btnType={'customButton-success'}  />
                                    <span className="px-3 font-weight-bold">123</span>
                                    </div>
                                <MDBIcon className="text-primary font-weight-bolder ml-2 pointer" icon="plus" />
                                </div>
                                <div className="card-narrow justify-content-between align-items-center shadow-type1 card-inline-flex d-flex  mt-1">
                                    <div>
                                    <KeywordButton text="تبلیغات خیلی خفن" btnType={'customButton-success'}  />
                                    <span className="px-3 font-weight-bold">123</span>
                                    </div>
                                <MDBIcon className="text-primary font-weight-bolder ml-2 pointer" icon="plus" />
                                </div>
                            </div>
                      </div>  
                      <div className="flex pl-4 position-relative " style={{top:"10rem"}}>
                            <MDBIcon icon="angle-double-left" className="text-primary" size="2x" />
                      </div>
                      <div className="card shadow-type1 flex-size-45">
                      <h6 className="font-weight-bolder text-right mt-2 pr-3">کلمات انتخاب شده</h6>
                      <hr />
                      <Accordion setActive="accordion_active"  className="mt-5 shadow-type1 accordion_shadow-light" toggle_content={<KeyworButton className="float-right" btnType="customButton-primary" />} body_content={"some content for your own sake brother"} />
                      <Accordion setActive="accordion_active"  className="mt-5 shadow-type1 accordion_shadow-light" toggle_content={<KeyworButton className="float-right" btnType="customButton-danger" />} body_content={"some content for your own sake brother"} />
                      <Accordion setActive="accordion_active"  className="mt-5 shadow-type1 accordion_shadow-light" toggle_content={<KeyworButton className="float-right" btnType="customButton-success" />} body_content={"some content for your own sake brother"} />
                          
                      </div>  
                </div>
            </div>
        )
    }
}
