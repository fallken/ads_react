import React, { Component } from 'react'

interface Props {
    isEnabled:boolean
}
interface State {
    isEnabled:boolean
}

export default class loadingDashboard extends Component<Props,State>{
    static defaultProps = {
        isEnabled:false
    }
    constructor(props:Props){
        super(props);
        this.state = {
            isEnabled:this.props.isEnabled
        }
    }
    render() {
        return (
            <div className={`loading_dashboard ${this.state.isEnabled?'loading_dashboard-active':''}`}>
                <div className="spinner-grow text-success" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-danger" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-warning" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        )
    }

    componentWillReceiveProps(nextProps:any)
    {
        this.setState({
            isEnabled:nextProps.isEnabled
        });
    }
}
