import React, { Component } from 'react';
import {MDBIcon} from 'mdbreact';

interface Props{
    btnType:string,
    text:string,
    status:string,
    className:string
}

export default class keywordButton extends Component<Props> {

    static defaultProps = {
        btnType:'customButton-primary',
        text:'تبلیغات',
        status:'شبیه',
        className:''
    }

    render() {
        return (
            <div className={`d-inline-flex customButton ${this.props.btnType} ${this.props.className}`}>
                <MDBIcon icon="times" className="customButton-icon  ml-2"/>
                <span className="customButton-title font-weight-bold mr-3">
                    {this.props.text}
                </span>
                <span className="customButton-info font-weight-bold mr-5">
                    {this.props.status}
                </span>
            </div>
        )
    }
}
