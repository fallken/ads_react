import React, { Component } from 'react';
import CurrencyChanger from './currencyChanger';
import { MDBIcon, MDBRow, MDBCol } from "mdbreact";
import OperationMessage from '../items/operationMessage';
export default class paymentInvoice extends Component {
    render() {
        return (
            <div>
               <MDBRow>
                <MDBCol size="9">
                        <OperationMessage />
                    <div className="card invoice shadow-type1 border_radius-biggerCurve noBorder">
                          <div className="invoice_header d-flex justify-content-between">
                              <div className="d-flex justify-content-start align-items-center flex-size-60">
                                  <div className="invoice_header-icon"><MDBIcon fab className="text-success" icon="airbnb" size="4x"/></div>
                                  <div className="invoice_header-title mr-2 font-weight-bold">گروه تبلیغاتی مای سهامی عام</div>
                              </div>
                              <div className="d-flex flex-column justify-content-between  flex-size-45">
                                  <div className="p-3">
                                      <span>صورتحساب شماره : 468784</span>
                                  </div>
                                  <div className="p-3 d-flex justify-content-end">
                                      <span className="ml-2">تاریخ صورت حساب :</span><span>08/07/2019</span>
                                  </div>
                              </div>
                          </div>  
                          <div className="invoice_details d-flex justify-content-between px-3 py-2">
                              <div className="d-flex flex-column flex-size-50 justify-content-between align-items-start">
                                  <div className="w-100 text-right p-1 font-weight-bold ">
                                    فروشنده: گروه تبلیغاتی مانی (سهامی خاص)
                                  </div>
                                  <div className="w-100 text-right p-1 font-weight-bold ">
                                    وبسایت : www.addup.ir
                                  </div>
                                  <div className="w-100 text-right p-1 font-weight-bold ">
                                      خیابان انتقلاب انهای ازادی جنب زندان
                                  </div>
                              </div>
                              <div className="d-flex flex-column flex-size-50 justify-content-between align-items-start">
                                     <div className="w-100 text-right p-1  font-weight-bold">
                                    خریدار : فرامرز ارسلانی 
                                  </div>
                                  <div className="w-100 text-right p-1  font-weight-bold">
                                    شماره ملی : 4790262812
                                  </div>
                                  <div className="w-100 text-right p-1  font-weight-bold">
                                      آدرس: تهران خیابان خراسان-کوچه خوش لهجه - پلاک 30
                                  </div>
                              </div>
                             
                          </div>
                          <div className="invoice_info mt-5">
                              <table>
                                     <tr className="bg-dark">
                                     <th className="text-white w-100">موارد فاکتور شده</th>
                                    </tr>

                                    <tr>
                                        <td className="d-flex justify-content-around w-100 px-5">
                                            <span>توضیحات</span>
                                            <span>مقدار</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="d-flex justify-content-around w-100 px-5">
                                            <span>شارژ کیف پول </span>
                                            <span>120000 هزار ریال</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="d-flex justify-content-around w-100 px-5">
                                            <span>جمع کل </span>
                                            <span>120000 هزار ریال</span>
                                        </td>
                                    </tr>
                               </table>
                              <table className="mt-3">
                                     <tr className="bg-dark">
                                     <th className="text-white w-100">موارد فاکتور شده</th>
                                    </tr>

                                    <tr>
                                        <td className="d-flex justify-content-around w-100 px-5">
                                            <span>توضیحات</span>
                                            <span>مقدار</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="d-flex justify-content-around w-100 px-5">
                                            <span>شارژ کیف پول </span>
                                            <span>120000 هزار ریال</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="d-flex justify-content-around w-100 px-5">
                                            <span>جمع کل </span>
                                            <span>120000 هزار ریال</span>
                                        </td>
                                    </tr>
                               </table>
                              </div>
                              <div className="d-flex mt-3 ">
                                  <button className="d-flex justify-content-between btn btn-primary text-white px-5 mr-auto border_radius-middleCurve">
                                    دریافت فایل
                                    
                                    <MDBIcon className="text-white mr-2" icon="download" size="lg" />
                                  </button>
                              </div>
                    </div>
                </MDBCol>
                 <MDBCol size="3" className="mr-auto">
                     <CurrencyChanger />
                 </MDBCol>

               </MDBRow> 
            </div>
        )
    }
}
