import React, { Component } from 'react'

interface Props{
    title:string,
    subject:string,
    info:string
}
interface State{

}
export default class headerComponent extends Component <Props,State>{
    static defaultProps = {
        title:'کمپین شماره یک ',
        subject:'حالت انتخاب شده',
        info:'ترافیک وبسایت - جستجو - zoorkhone.com',
    }
    render() {
        return (
            <div className="infoHeader mb-5">
                <h4 className="infoHeader-title font-weight-bold p-2">
                    {this.props.title}
                </h4>
                <div className="infoHeader-subject  font-weight-bolder mr-5 p-2">
                    {this.props.subject}
                </div>
                <div className="infoHeader-info  font-weight-bolder mr-3 p-2">
                    {this.props.info}
                </div>
            </div>
        )
    }
}
