import React, { Component } from 'react'
import CardSimple from '../../items/cardSimple';



export default class test extends Component {
    render() {
        return (
            <div className="d-flex flex-column w-100">
                <div className="form-group p-3">
                    <label htmlFor="example3" className="text-right w-100 font-weight-bold">نام کمپین <span className="has-text-primary"> &#169; </span></label>
                    <input  className="form-control form-control-lg w-50 custom_input-one" placeholder="نام مورد نظر خود را وارد کنید" type="text" id="example3" />
                </div>
                 <label htmlFor="example3" className="text-right w-100 font-weight-bold p-3">دامنه جستجو را انتخاب کنید</label>

                <div className="d-flex flex-row">
                    <CardSimple image={require('../../../assets/images/googleAds.png')} />
                    <CardSimple classes="m-r-30" isActive={true} image={require('../../../assets/images/googleAds.png')} />
                </div>
            </div>
        )
    }
}
