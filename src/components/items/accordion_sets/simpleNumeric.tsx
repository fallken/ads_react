import React, { Component } from 'react'

interface Props {
    number:number,
    title:string,
    subject:string,
    data:any,
    sideOption:any,
    digitColor:string,
    sideOptionClass:string
}

interface State {
    //state goes here 
}

export default class simpleNumeric extends Component<Props , State > {

    static defaultProps={
        number:12,
        title:'نام کمپین و دامنه جست و جو',
        subject:'حالت انتخاب شده:',
        data:'کمپین شماره یک   جستجو ',
        digitColor:'bg-success',
        sideOption:'',
        sideOptionClass:''
    }

    render() {
        return (
            <div className="simpleNumeric">
                <div className="simpleNumeric_number-container">
                    <div className={`simpleNumeric_number ${this.props.digitColor}`}>
                        <span className="simpleNumeric_number-digit ">
                        {this.props.number}
                        </span>
                    </div>
                </div>
               
                <div className="simpleNumeric_info">
                    <div className="simpleNumeric_info-title">
                        {this.props.title}
                    </div>
                    <div className="simpleNumeric_info-text">
                        <div className="simpleNumeric_info-text_subject">
                            {this.props.subject}
                        </div>
                        <div className="simpleNumeric_info-text_data m-r-40">
                           {this.props.data}
                        </div>
                    </div>
                </div>
                <div className={`simpleNumeric-left ${this.props.sideOptionClass}`}>
                    {this.props.sideOption}
                </div>
            </div > 
        )
    }
}


