import React, { Component } from 'react'
import { getDiffieHellman } from 'crypto';

interface Props {
    slots:number,
    fill:number
}
interface State {
    fillColorClass:string,
    fillStatus:string
}
export default class progressBar extends Component<Props , State>{

    static defaultProps = {
        slots:15,
        fill:12
    }
    constructor(props:Props){
        super(props);
        this.state = {
            fillColorClass:'fill-danger',
            fillStatus:'ضعیف'
        }
    }
    
    setFillInfo = (fillval:number = this.props.fill )=>{
        console.log('function called');
        console.log(`the fill val is ${this.props.fill}`);
        if(fillval > Math.ceil(this.props.slots/2)){
            console.log('function called 1');
            this.setState({
                fillColorClass:'fill-success',
                fillStatus:'عالی'
            })
        }else if (fillval ===  Math.ceil(this.props.slots/2)){
            console.log('function called 2');
            this.setState({
                fillColorClass:'fill-warning',
                fillStatus:'متوسط'
            })
        }else if (fillval <  Math.ceil(this.props.slots/2)){
            console.log('function called 3');
            this.setState({
                fillColorClass:'fill-danger',
                fillStatus:'ضعیف'
            })
        }
    }

    getSlots = ()=>{
        let result = [];
        let checkFill = (index:number)=>{
            if(index <= this.props.fill){
                return this.state.fillColorClass;
            }
        }

        for (let i = 1; i <= this.props.slots; i++) {
            result.push(
            <div key={i} className={`progressBar-slots_item flex-fill  ${checkFill(i)}`}>
            </div>
            );
        } ;
        return result;
    }

    render() {
        return (
            <div className="progressBar ">
                <div className="progressBar-status ml-2 font-weight-bold">
                    {this.state.fillStatus}
                </div>
                <div className={`progressBar-slots flex-row-reverse`}>
                    {
                        this.getSlots()
                    }
                </div>
            </div>
        )
    }

    componentWillReceiveProps(nextProps:any)
    {
        this.setFillInfo(nextProps.fill);
        this.forceUpdate();
    }
}
