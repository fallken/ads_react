import React, { Component, Fragment } from 'react'
import { MDBBtn, MDBIcon } from "mdbreact";

export default class testHeader extends Component {
    render() {
        return (
            <Fragment>
                    <MDBBtn color="primary">
                        <MDBIcon icon="magic" className="mr-1" /> Left
                    </MDBBtn>
                    <MDBBtn color="default">
                        Right <MDBIcon icon="magic" className="ml-1" />
                    </MDBBtn>
            </Fragment>
        )
    }
}
