import React, { Fragment } from 'react'

interface Props{
    classes:string,
    image:string,
    isActive:boolean,
    isHorizontal:boolean,
    title:string,
    desc:string
}

const cardSimple = (props:Props)=>{
    return (
        <div className={`card cardSimple ${props.isHorizontal?'cardSimple-horizontal':''}  ${props.classes} ${props.isActive?'cardSimple-active':''}`}>
            {
                !props.isHorizontal?(
                    <Fragment>
                        <div className="cardSimple_title">
                            {props.title}
                        </div>
                        <div className="cardSimple_img">
                            <img src={props.image} alt="card img"/>
                        </div>
                        <div className="cardSimple_desc">
                            {props.desc}
                        </div>
                    </Fragment>
                ):(
                    <Fragment>
                        <div className="cardSimple_img">
                            <img src={props.image} alt="card img"/>
                        </div>
                        <div className="cardSimple-horizontalContainer">
                            <div className="cardSimple_title">
                                {props.title}
                            </div>
                            <div className="cardSimple_desc">
                                {props.desc}
                            </div>
                        </div>
                    </Fragment>
                )
            }
        </div>
    )
}

cardSimple.defaultProps={
    classes:'',
    image:require('../../assets/images/card_img.png'),
    isActive:false,
    isHorizontal:false,
    title:'جستجو',
    desc:'نمایش تبلیغ مشابه نتایج جستجو'
}

export default cardSimple;