import React, { Component } from 'react'
import { MDBIcon} from "mdbreact";

interface Props{
    colorClass:string,
    text1:string,
    text2:string,
}


export default class operationMessage extends Component<Props> {
    static defaultProps={
        colorClass:'btn-success',
        text1:'عملیات موفق',
        text2:'شارژ کیف پول با موفقیت انجام شد',
    }


    render() {
        return (
            <label  className={`btn w-100 border_radius-middleCurve text-white mb-3 d-flex justify-content-between ${this.props.colorClass}`}>
                <div className="d-flex align-items-center">
                    <MDBIcon icon="plus" size="lg" />
                    <span className="mr-2">{this.props.text1}</span>
                </div>
                <div className="text-right flex-size-80">
                    <span>{this.props.text2}</span>
                </div>
                <div className="d-flex align-items-center">
                    <MDBIcon  className="hoverable" icon="times" size="lg" />
                </div>
            </label>
        )
    }
}
