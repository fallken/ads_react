import React, { Component } from 'react'


interface Props {
    stages:number,
    active:any
}
export default class stageCounter extends Component<Props> {
    static defaultProps = {
        stages:5,
        active:2
    }
    render() {
        let items = [];
        for(let i=1;i<=this.props.stages;i++){
            items.push(
            <div key={i} className="stageCounter-item">
                    <div className={`stageCounter-number font-weight-bold ${this.props.active && this.props.active == i?'bg-primary':''}`}>
                        {i}
                    </div>
                    {
                        i !== this.props.stages?(
                            <div className="stageCounter-seperator">
                                <div className={this.props.active && this.props.active == i?'bg-primary':''}></div>
                            </div>
                        ):''
                    }
            </div>
            )
        }
        return (
            <div className="stageCounter mb-4">
                {
                    items
                }
            </div>
        )
    }
}
