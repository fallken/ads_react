import React, { Component } from 'react'
import {MDBIcon,MDBInput} from 'mdbreact';

export default class currencyChanger extends Component {
    render() {
        return (
                 <div className="card shadow-type1" style={{borderRadius:"10px"}} >
                                <h6 className="font-weight-bold d-flex justify-content-start align-items-center text-right">
                                    <span className="currency_converter-icon ml-3">
                                        <MDBIcon className="text-white" size="lg" icon="lira-sign" />
                                    </span>
                                     لیر ترکیه
                                </h6>
                                <div className="currency_converter d-flex justify-content-between p-1 px-3">
                                    <div className="currency_input d-flex flex-column justify-content-between flex-size-40">
                                        <span className="pb-1 text-small text-right" >لیر ترکیه</span>
                                        <input type="text" style={{width:"100%",backgroundColor:"transparent",border:"none",borderBottom:"1px solid grey"}}/>
                                    </div>
                                    <div className="currency_buttons flex-size-15 bg-primary py-1 px-2 d-flex flex-column">
                                        <MDBIcon className="text-white p-2" icon="angle-left" size="lg" />
                                        <MDBIcon className="text-white p-2" icon="angle-right" size="lg" />
                                    </div>
                                    <div className="currency_result d-flex flex-size-40 flex-column justify-content-between">
                                        <span className="justify-self-end text-right pb-1 mr-1 text-small">ریال ایران</span>
                                        <span className="font-weight-bold">12.203</span>
                                    </div>
                                </div>
                                <h6 className="font-weight-bold text-right mt-4">قیمت هر واحد لیر در بازار تهران</h6>

                                <div className="p-4 shadow-type1 currency_stat mt-1 d-flex  justify-content-between bg-warning_light" style={{borderRadius:"10px",borderBottomRightRadius:"0"}}>
                                    <span>قیمت روز</span>
                                    <span className="">12.205 ریال</span>
                                 </div>
                                <div className="p-4 shadow-type1 currency_stat mt-1 d-flex  justify-content-between">
                                    <span>دیروز</span>
                                    <span className="text-success"> <MDBIcon icon="arrow-up" /> 12.05 ریال</span>
                                 </div>
                                <div className="p-4 shadow-type1 currency_stat mt-1 d-flex  justify-content-between">
                                    <span>دو روز پیش</span>
                                    <span className="text-danger"> <MDBIcon icon="arrow-down" /> 12.05 ریال</span>
                                 </div>
                                <div className="p-4 shadow-type1 currency_stat mt-1 d-flex  justify-content-between"  style={{borderBottomRightRadius:"10px",borderBottomLeftRadius:"10px"}} >
                                    <span>سه روز پیش</span>
                                    <span className="text-success"> <MDBIcon icon="arrow-up" /> 12.05 ریال</span>
                                 </div>
                            </div>
        )
    }
}
