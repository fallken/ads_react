import React, {useState, useRef} from 'react';
import {MDBIcon} from 'mdbreact';

interface Props {
    toggle_content: any,
    body_content: any,
    border_radius: string,
    has_border_buttom_radius:boolean,
    setActive: string,
    className:string,
    toggleClassName:string,
}

const Accordion = (props: Props) => {


    const [setActive, setActiveState] = useState("");
    const [setHeight, setHeightState] = useState("0px");
    const [setRotate, setRotateState] = useState("accordion_toggle-icon");


    const content:any = useRef(null);


    function toggleAccordion(e:any) {

        e.preventDefault();
        setActiveState(setActive === "" ? "accordion_active" : "");

        setHeightState(
            setActive === "accordion_active" ? "0px" : `${content.current.scrollHeight}px`
        );

        setRotateState(
            setActive === "accordion_active" ? "accordion_toggle-icon" : "accordion_toggle-icon accordion_toggle-icon_rotate"
        );
    }

    return (
        <div className={`m-t-5  ${props.className}`}>
            <div className="accordion">
                <div className={`accordion_toggle ${props.toggleClassName}`}
                     style={{
                         borderTopLeftRadius: props.border_radius,
                         borderTopRightRadius: props.border_radius,
                         borderBottomRightRadius : props.has_border_buttom_radius?props.border_radius:'',
                         borderBottomLeftRadius : props.has_border_buttom_radius?props.border_radius:''
                     }}>
                    <div className="accordion_toggle-content">
                        {props.toggle_content}
                    </div>
                    <div className="accordion_toggle-toggler" onClick={toggleAccordion}>
                        <a href="#" className="caretItem">
                            <MDBIcon className={`m-t-5 p-l-10 ${setRotate}`} icon="angle-down" size="2x"/>
                        </a>
                    </div>
                </div>
                <div
                    ref={content}
                    style={{maxHeight: `${setHeight}`}}
                    className={`accordion_content ${setActive}`}
                >
                    <div className="display-flex">
                        {props.body_content}
                    </div>
                </div>
            </div>
        </div>
    )
}
Accordion.defaultProps = {
    toggle_content: '',
    body_content: '',
    border_radius: '3px',
    has_border_buttom_radius:false,
    setActive: '',
    className:'',
    toggleClassName:'',

}

export default Accordion;