import React, { Component } from 'react';
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import SimpleNumeric from './items/accordion_sets/simpleNumeric';
import {MDBIcon,MDBInput} from 'mdbreact';
import Accordion from './items/Accordion';    
import MyTest from './items/accordion_sets/test';
import CurrencyChanger from './items/currencyChanger';
export default class paymentPage extends Component {
    render() {
        return (
                <MDBRow>
                    <MDBCol size="9" className="d-flex flex-column just-content-start  pl-2 pr-0">
                        <h6 className="font-weight-bold text-right">شارژ حساب کاربری آداپ</h6>
                        <span className="text-primary font-weight-bolder w-100 text-right pb-3 mt-1">پس از شارژ حساب کاربری میتوانید از ان برای بودجه تمام درگاه های تبلیغاتی استفاده کنید</span>
                        <div className="accordion" >
                           <div className={`accordion_toggle`}   style={{
                                borderTopLeftRadius: "10px",
                                borderTopRightRadius: "10px",
                            }}>
                                <div className="accordion_toggle-content">

                                    <div className="simpleNumeric">
                                            <div className="simpleNumeric_number-container">
                                                <div className={`simpleNumeric_number bg-success `}>
                                                    <span className="simpleNumeric_number-digit">
                                                        1
                                                    </span>
                                                </div>
                                            </div>
                                            <div className="simpleNumeric_info">
                                                <div className="simpleNumeric_info-title">
                                                    انتخاب کیف پول
                                                </div>
                                                <div className="simpleNumeric_info-text">
                                                    <div style={{fontSize:".9rem"}} className="font-weight-bold  simpleNumeric_info-text_data">
                                                        یکی از کیف پول های خود را انتخاب کنید
                                                    </div>
                                                </div>
                                            </div>
                                            <div className={`selectBox simpleNumeric-left flex-size-60`}>
                                                        <select className=" browser-default custom-select">
                                                        <option>کیف پول اداپ</option>
                                                        <option value="1">سریع</option>
                                                        <option value="2">بیشترین کلیک</option>
                                                        <option value="3">بیشترین مشاهده</option>
                                                    </select>
                                            </div>
                                    </div > 

                                    </div>
                            </div>
                        </div>
                        <div className="accordion mt-1" >
                           <div className={`accordion_toggle`}  style={{
                                borderRadius: "3px",
                            }}>
                                <div className="accordion_toggle-content">
                                    
                                <div className="simpleNumeric">
                                            <div className="simpleNumeric_number-container">
                                                <div className={`simpleNumeric_number bg-success `}>
                                                    <span className="font-weight-bold simpleNumeric_number-digit ">
                                                        2
                                                    </span>
                                                </div>
                                            </div>
                                            <div className="simpleNumeric_info">
                                                <div className="simpleNumeric_info-title">
                                                    مبلغ واریزی
                                                </div>
                                                <div className="simpleNumeric_info-text">
                                                    <div  style={{fontSize:".9rem"}} className="simpleNumeric_info-text_data font-weight-bold">
                                                        حداقل واریز 10000 ریال و حداکثر 100000000 ریال
                                                        </div>
                                                </div>
                                            </div>
                                            <div className={`simpleNumeric-left flex-size-60`}>
                                                <input  className="form-control form-control-lg custom_input-one" placeholder="مثال 10000 تومان" type="text" id="example3" />
                                            </div>
                                </div > 

                                </div>
                            </div>
                        </div>
                        <div className="accordion mt-1" >
                           <div className={`accordion_toggle`}  style={{
                                borderRadius: "3px",
                            }}>
                                <div className="accordion_toggle-content">
                                    <div className="simpleNumeric">
                                                <div className="simpleNumeric_number-container">
                                                    <div className={`simpleNumeric_number bg-success `}>
                                                        <span style={{fontSize:".9rem"}} className="font-weight-bold simpleNumeric_number-digit ">
                                                            3
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="simpleNumeric_info">
                                                    <div className="simpleNumeric_info-title">
                                                        طریقه پرداخت
                                                    </div>
                                                    <div className="simpleNumeric_info-text">
                                                        <div  style={{fontSize:".9rem"}} className="simpleNumeric_info-text_data font-weight-bold">
                                                            یکی از درگاه های پرداخت را انتخاب کنید
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className={`selectBox simpleNumeric-left flex-size-60`}>
                                                        <select className=" browser-default custom-select">
                                                    <option>درگاه اسان پرداخت</option>
                                                    <option value="1">سریع</option>
                                                    <option value="2">بیشترین کلیک</option>
                                                    <option value="3">بیشترین مشاهده</option>
                                                    </select>
                                                </div>
                                        </div > 
                                    </div>
                            </div>
                        </div>
                        <Accordion toggleClassName="bg-primary_light" toggle_content={
                        <div className="d-flex justify-content-between">
                            <span className="text-danger">مبلغ شارژ قابل برگشت نخواهد بود</span>
                            <span className="text-primary">نمایش تنظیمات بیشتر</span>
                        </div>
                        } body_content={<MyTest />} />

                        <div className="d-flex justify-content-end mt-4 ">
                            <button className="shadow-type1 px-5 btn btn-success btn-lg text-white" style={{borderRadius:"9px"}}>پرداخت وجه</button>
                        </div>
                    </MDBCol>
                    <MDBCol size="3" className="pl-0 pr-1" >
                            <CurrencyChanger />
                    </MDBCol>
                </MDBRow>
        )
    }
}
