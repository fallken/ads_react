import React, { Component } from 'react';
import { MDBIcon } from "mdbreact";
import logo from '../../assets/images/logo.png';
import {connect} from 'react-redux';
import {setActiveItem,setActiveOption} from '../../redux/actions/dashboard';
import {Link} from 'react-router-dom';

interface MyProps {
    dashboard:any,
    dispatch:any,
    page_id:string
}
interface MyState {
   
}

class sidebar extends Component<MyProps , MyState> {

    constructor(props:MyProps){
        super(props);
        if(this.props.page_id){
            this.setActive(this.props.page_id,true)//passing page_id as option id 
        }
    }
    
    static defaultProps = {
        page_id:''
    }

    setActive = (id:string,isOption:boolean=false):void=>{
        if(isOption){
            this.props.dispatch(setActiveOption(id))
        }else{
            this.props.dispatch(setActiveItem(id))
        }
    }
    getIcon = (sideItem:any):any=>{
        switch(sideItem.icon_type){
             case 'fab':
                 return <MDBIcon fab icon={sideItem.icon} size={sideItem.icon_size} />
             case 'far':
                return  <MDBIcon far icon={sideItem.icon} size={sideItem.icon_size} />
             default:
                 return <MDBIcon  icon={sideItem.icon} size={sideItem.icon_size} />
        }
    }

    render() {
        return (
                <div>
                 <div className="sideBar">
                <div className="sideBar_content">
                    <div className="sideBar_content-logo">
                        <img src={logo} height="120" width="120" alt={"logo"} />
                        <h3>آژانس تبلیغاتی ادآپ</h3>


                        {/* <img src={require("../../assets/images/imgb.svg")}   alt={"logo"} /> */}

                    </div>
                    <div className="sideBar_content_list">
                            <ul className="sideBar_content_list-items">
                                {
                                    this.props.dashboard.items.map((object:any,index:number)=>{
                                        var icon = this.getIcon(object)
                                        return (
                                        <Link to={object.base_option_id} key={object.id} className={`sideBar_content_list-item ${this.props.dashboard.activeItem === object.id ? 'sideBar_content_list-item-selected':'' }`} onClick={()=>{this.setActive(object.id)}}>
                                            <div></div>
                                            {icon}
                                            <span className="m-r-15">{object.name}</span>
                                        </Link>
                                        )
                                    })
                                }
                            </ul>
                            <div className="sideBar_content_list-options">
                                    <ul className="sideBar_options-items">
                                        {
                                            this.props.dashboard.options.map((object:any,index:number)=>{
                                                var icon = this.getIcon(object)
                                                if(object.parent === this.props.dashboard.activeItem){
                                                    return (
                                                    <Link  onClick={()=>{this.setActive(object.id,true)}} to={object.id} key={index} className={`sideBar_options-item ${this.props.dashboard.activeOption === object.id ? 'sideBar_options-item-selected':''  }`}>
                                                        {icon}
                                                        <span className="m-r-15">{object.name}</span>
                                                    </Link>
                                                    )
                                                }
                                            })
                                        }
                                    </ul>
                            </div>
                    </div>
                    <div className="sideBar_content_bottom">

                    </div>
                </div>
            </div>
            </div>
        )
    }

    componentWillReceiveProps(nextProps:any)
    {
        if (this.props.dashboard.activeItem !== nextProps.dashboard.activeItem)
        {
            this.forceUpdate();
        }
    }
}

const mapStateToProps = (state:any)=>({
    dashboard:state.dashboard
});



export default connect(mapStateToProps)(sidebar);