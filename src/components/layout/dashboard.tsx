import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Sidebar from './sidebar';
import Navbar from './navbar';
import {Link} from 'react-router-dom';
import { MDBContainer} from "mdbreact";
import {connect} from 'react-redux';
import AccessDenied from '../accessDenied';
import LoadingDashboardComponent from '../loadingDashboardComponent';
import {setActiveOption} from '../../redux/actions/dashboard';
import ImageGallery from '../items/imageGallery';


interface MyProps {
    page:any,
    page_id:string,
    dashboard:any,
    dispatch:any
}
interface MyState {
    name:string
}

class dashboard extends Component<MyProps,MyState> {
    pageName = this.props.page?this.props.page.type.name:'home';

    constructor(props:MyProps){
        super(props);
        console.log(`the page is ${this.pageName}`);
        // if(this.checkPageAccess(this.page)){
        //     this.props.dispatch(setActiveOption(this.page))
        // }
    }
    static defaultProps={
        page:'',
        page_id:''
    }

    checkPageAccess = (page:string)=>{
        if(page === 'window'){
            return false;
        }else{
            return true;
        }
    }

    render() {
        return (
            <div>
                 <div id="adsporContainer">
                    <Sidebar page_id={this.props.page_id} />
                    <div className="content">
                        <Navbar />
                        {/* rendering the page passed from react router */}
                        <MDBContainer className="mb-5 position-relative" >
                            <LoadingDashboardComponent />
                            <ReactCSSTransitionGroup
                                transitionAppear={true}
                                transitionAppearTimeout={600}
                                transitionEnterTimeout={600}
                                transitionLeaveTimeout={200}
                                transitionName={'SlideIn'}
                            >
                                {
                                   this.props.page?(
                                   this.checkPageAccess(this.pageName)?this.props.page:<AccessDenied />
                                   ):(
                                       <ImageGallery />
                                   )
                                }
                            </ReactCSSTransitionGroup>
                        </MDBContainer>
                        {/* end of rendering page the user wants */}
                        </div>
                    </div>
            </div>
        )
    }
}
const mapStateToProps = (state:any)=>({
    dashboard:state.dashboard
});


export default connect(mapStateToProps)(dashboard);