import React, { Component } from 'react'
import { MDBBtn,MDBIcon } from "mdbreact";
import {Link} from 'react-router-dom';
export default class navbar extends Component {
    render() {
        return (
            <div>
                <div id="topNav_before">
                </div>
                <div id="topNav">   
                     <div className="user_info">
                         <div className="user_info-profile">
                             <div className="user_info-profile_avatar">
                                 <img src="https://picsum.photos/id/1011/40/40" alt="user profile"/>
                             </div>
                             <div className="user_info-profile_info">
                                 <span className="user_info-profile_info-name">
                                    فرامرز ارسلانی
                                 </span>
                                 <span className="user_info-profile_info-id">
                                    123-469-822
                                 </span>
                             </div>
                             <div className="user_info-profile_more">
                                    <a href="#" className="caretItem">
                                            <MDBIcon icon="angle-down" size="lg"  className="m-t-5 p-l-10" />
                                    </a>
                                    <div className="user_info-profile_more-options">
                                        <ul className="user_info-profile_more-options_list">
                                            <li className="user_info-profile_more-options_list-items">
                                                <a href="#" className="">گزینه اول</a>
                                            </li>
                                            <li className="user_info-profile_more-options_list-items">
                                                <a href="" className="">گزینه دوم</a>
                                            </li>
                                            <hr />
                                            <li className="user_info-profile_more-options_list-items">
                                                <a href="" className="">سوم</a>
                                            </li>
                                            <li className="user_info-profile_more-options_list-items">
                                                <a href="" className="">چهارم</a>
                                            </li>
                                        </ul>
                                    </div>
                             </div>

                         </div>
                         <div className="user_info-additional">
                            <div className="user_info-additional_messages m-r-5">
                                <MDBIcon  icon="comments" size="lg" />
                            </div>
                            <div className="user_info-additional_notifications ">
                                <MDBIcon icon="bell" size="lg" />
                             </div>
                             <div className="user_info-additional_setting ">
                                <MDBIcon icon="cog" size="lg" />
                             </div>
                         </div>
                        
                     </div>   
                     <div className="user_actions m-t-5">
                         <div className="user_actions-button">
                             <Link to="file_user">
                                <MDBBtn outline rounded color="info" className="navBtn">
                                <MDBIcon fab icon="google"  />
                                    <span className="m-r-5">
                                    ایجاد کمپین 
                                    </span>
                                    </MDBBtn>
                             </Link>
                         </div>
                         <div className="user_actions-button">
                         <Link to="file_user">
                         <MDBBtn outline rounded color="info" className="navBtn">
                         <MDBIcon  icon="wallet"  />
                         <span className="m-r-5">
                         شارز حساب
                         </span>
                             </MDBBtn>
                             </Link>
                         </div>
                     </div>
                </div>
                <div id="topNav-secondary">
                    <div className="topNav-navigation m-r-25">
                        <div className="topNav-navigation_item">
                            <MDBIcon icon="home" size="lg" className="has-text-primary m-l-10" />
                            <span>داشبود</span>
                        </div>
                    </div>
                    <div className="topNav-calender">
                    </div>
                </div>

            </div>
        )
    }
}
