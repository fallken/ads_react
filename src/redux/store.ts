import {createStore , combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';
import DashboardReducer from './reducers/DashboardReducer';
import AuthReducer from './reducers/AuthReducer';
import NotificationReducer from './reducers/NotificationReducer';

export default ()=>{
    const store = createStore(
        combineReducers({
            dashboard:DashboardReducer,
            auth:AuthReducer,
            notification:NotificationReducer
        }),composeWithDevTools(applyMiddleware(thunk))
    )
    return store;
}