import {SET_ACTIVE_ITEM,SET_ACTIVE_Option} from '../consts/dashboard'



export const setActiveItem = (id:string)=>({
    type:SET_ACTIVE_ITEM,
    id
});

//set active option and active item of its parent 
export const setActiveOption  = (id:string)=>({
    type:SET_ACTIVE_Option,
    id
}); 