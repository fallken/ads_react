import {SET_ACTIVE_ITEM,SET_ACTIVE_Option} from '../consts/dashboard'
import sidebar_items from '../routes/sidebar_items';
import sidebar_options from '../routes/sidebar_options';


const defaultState = {
        activeItem:'home',
        activeOption:'basket',
        items:sidebar_items,
        options:sidebar_options
}

const getItem = (id:string)=>{
    let item:any = defaultState.items.find(object=>object.id === id);
    return item ; 
}
const getParentItem = (optionId:string)=>{
    
    let option:any = defaultState.options.find(object=>object.id === optionId);

    console.log(`the option is :`);
    console.log(option);
    

    let item:any = getItem(option.parent);

    return item.id;
}

export default (state:object=defaultState,action:any )=>{
    switch(action.type){
        case SET_ACTIVE_ITEM:
            let item = getItem(action.id);
            return {...state,activeItem:action.id,activeOption:item.base_option_id};
        case SET_ACTIVE_Option:
            return {...state,activeOption:action.id,activeItem:getParentItem(action.id)};
        default:
            return state;
    }
}