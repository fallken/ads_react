import React from 'react';
import logo from './logo.svg';
import Dashboard from './components/layout/dashboard';
import TestMe from './components/testMe';
import Campaigns from './components/google/campaigns';
import CreateCampaign from './components/google/createCampaign';
import LoadingDashboard from './components/loadingDashboard';
import PaymentInvoice from './components/items/paymentInvoice';
import CreateAdgroups from './components/google/createAdgroups';
import UserWallet from './components/userWallet';
import PaymentPage from './components/paymentPage';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './assets/css/main.css';
import { BrowserRouter as Router,Route } from 'react-router-dom';
//redux section
import {Provider} from 'react-redux';
import Store from './redux/store';

const store:any = Store();
// const reduxState:any = store.getState();

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
            <LoadingDashboard isEnabled={false} />
            {/* home section routings */}
          <Route path="/" exact component={() => <Dashboard page_id="window_home" page={<PaymentPage />} />} ></Route>
            <Route path="/home" exact component={()=><Dashboard page_id="window_home" page={<PaymentPage/>} />} ></Route>
            <Route path="/window_home" exact component={()=><Dashboard page={<PaymentPage/>}  page_id="window_home" />} ></Route>
            <Route path="/file_home" exact component={()=><Dashboard page_id="file_home" page={<Campaigns />}  />} ></Route>
            <Route path="/basket_home" exact component={()=><Dashboard page_id="basket_home" page={<CreateAdgroups />} />}></Route>
            <Route path="/calender_home" exact component={()=><Dashboard page_id="calender_home" page={<PaymentInvoice />} />}></Route>
            <Route path="/user_home" exact component={()=><Dashboard page_id="user_home" page={<UserWallet />} />}></Route>
            <Route path="/chart_home" exact component={()=><Dashboard page_id="chart_home" page={<TestMe />} />}></Route>
            {/* google section routing */}
            <Route path="/window_google" exact component={()=><Dashboard page_id="window_google" />} ></Route>
            <Route path="/file_google" exact component={()=><Dashboard page_id="file_google" page={<CreateCampaign />}  />} ></Route>
            <Route path="/basket_google" exact component={()=><Dashboard page_id="basket_google" page={<TestMe />} />}></Route>
            <Route path="/calender_google" exact component={()=><Dashboard page_id="calender_google" page={<TestMe />} />}></Route>
            <Route path="/user_google" exact component={()=><Dashboard page_id="user_google" page={<TestMe />} />}></Route>
            <Route path="/chart_google" exact component={()=><Dashboard page_id="chart_google" page={<TestMe />} />}></Route>
            {/* user section routing */}
            <Route path="/window_user" exact component={()=><Dashboard page_id="window_user" />} ></Route>
            <Route path="/file_user" exact component={()=><Dashboard page_id="file_user" page={<Campaigns />}  />} ></Route>
            <Route path="/basket_user" exact component={()=><Dashboard page_id="basket_user" page={<TestMe />} />}></Route>
            <Route path="/calender_user" exact component={()=><Dashboard page_id="calender_user" page={<TestMe />} />}></Route>
            <Route path="/user_user" exact component={()=><Dashboard page_id="user_user" page={<TestMe />} />}></Route>
            <Route path="/chart_user" exact component={()=><Dashboard page_id="chart_user" page={<TestMe />} />}></Route>
            {/* support section routing  */}
            <Route path="/chart_support" exact component={()=><Dashboard page_id="chart_support" page={<TestMe />} />}></Route>
            <Route path="/file_support" exact component={()=><Dashboard page_id="file_support" page={<TestMe />} />}></Route>
            <Route path="/basket_support" exact component={()=><Dashboard page_id="basket_support" page={<TestMe />} />}></Route>
            {/* wallet section routing  */}
            <Route path="/chart_wallet" exact component={()=><Dashboard page_id="chart_wallet" page={<TestMe />} />}></Route>
            <Route path="/file_wallet" exact component={()=><Dashboard page_id="file_wallet" page={<TestMe />} />}></Route>
            <Route path="/basket_wallet" exact component={()=><Dashboard page_id="basket_wallet" page={<TestMe />} />}></Route>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
